#include <catch2/catch.hpp>

#include "bst_base.hpp"
#include "util.hpp"

TEMPLATE_PRODUCT_TEST_CASE("happy pass", "[bst]", (bst_base), ((int, int)))
{
    TestType tree;
    std::vector vec{34, 22, 7, 21, 33, 23, 86, 56, 100, 101};
    /*
     *                   34
     *               /        \
     *              22         86
     *            /   \       /  \
     *          7      33    56   100
     *           \     /            \
     *           21    23           101
     */
    auto value = 666;
    for (const auto &key : vec)
        tree.insert(key, value);

    for (const auto key : vec)
        CHECK(tree.search(key)->m_key == key);

    SECTION("remove node without children")
    {
        tree.remove(56);
        CHECK(tree.search(56) == nullptr);
        CHECK(tree.search(86)->m_left == nullptr);

        tree.remove(101);
        CHECK(tree.search(101) == nullptr);
        CHECK(tree.search(100)->m_right == nullptr);

        tree.remove(23);
        CHECK(tree.search(23) == nullptr);
        CHECK(tree.search(33)->m_left == nullptr);

        tree.remove(21);
        CHECK(tree.search(21) == nullptr);
        CHECK(tree.search(7)->m_right == nullptr);
    }

    SECTION("remove node with one child")
    {
        tree.remove(7);
        CHECK(tree.search(7) == nullptr);
        CHECK(tree.search(22)->m_left->m_key == 21);
        CHECK(tree.search(21)->m_parent->m_key == 22);

        tree.remove(33);
        CHECK(tree.search(33) == nullptr);
        CHECK(tree.search(22)->m_right->m_key == 23);
        CHECK(tree.search(23)->m_parent->m_key == 22);

        tree.remove(100);
        CHECK(tree.search(100) == nullptr);
        CHECK(tree.search(86)->m_right->m_key == 101);
        CHECK(tree.search(101)->m_parent->m_key == 86);
    }

    SECTION("remove node having both children")
    {
        tree.remove(86);
        CHECK(tree.search(86) == nullptr);
        CHECK(tree.search(100)->m_left->m_key == 56);
        CHECK(tree.search(100)->m_right->m_key == 101);
        CHECK(tree.search(100)->m_parent->m_key == 34);
        CHECK(tree.search(56)->m_parent->m_key == 100);
        CHECK(tree.search(101)->m_parent->m_key == 100);
        CHECK(tree.search(34)->m_right->m_key == 100);

        tree.remove(22);
        CHECK(tree.search(22) == nullptr);
        CHECK(tree.search(23)->m_left->m_key == 7);
        CHECK(tree.search(23)->m_right->m_key == 33);
        CHECK(tree.search(23)->m_parent->m_key == 34);
        CHECK(tree.search(7)->m_parent->m_key == 23);
        CHECK(tree.search(33)->m_parent->m_key == 23);
        CHECK(tree.search(34)->m_left->m_key == 23);
    }

    SECTION("remove root")
    {
        tree.remove(34);
        CHECK(tree.search(34) == nullptr);
        CHECK(tree.m_root->m_key == 56);
        CHECK(tree.search(56)->m_left->m_key == 22);
        CHECK(tree.search(56)->m_right->m_key == 86);
        CHECK(tree.search(22)->m_parent->m_key == 56);
        CHECK(tree.search(86)->m_parent->m_key == 56);
        CHECK(tree.search(86)->m_left == nullptr);
    }
}
