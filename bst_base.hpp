#pragma once
#include <iostream>

template <typename K, typename V>
class bst_base
{
    public:
        bst_base() : m_root(nullptr) {}

        struct node_t
        {
            K m_key;
            V m_value;
            node_t *m_left;
            node_t *m_right;
            node_t *m_parent;

            node_t() = delete;
            node_t(K key, V value, node_t *parent) : m_key(key), m_value(value), m_left(nullptr), m_right(nullptr), m_parent(parent) {}
            ~node_t() = default;
        };

        node_t *m_root;


        void insert(const K &key, const V &value)
        {
            helper_insert(m_root, m_root, key, value);
        }

        node_t *search(const K &key)
        {
            return helper_search(m_root, key);
        }

        void remove(const K &key)
        {
            auto node = helper_search(m_root, key);
            if (node == nullptr)
                return;

            if (node->m_left == nullptr && node->m_right == nullptr)
            {
                if (node != m_root)
                {
                    if (node->m_parent->m_left == node)
                        node->m_parent->m_left = nullptr;
                    else
                        node->m_parent->m_right = nullptr;
                }
                else
                {
                    m_root = nullptr;
                }
            }
            else if (node->m_left && node->m_right)
            {
                auto successor = node->m_right;
                while (successor->m_left != nullptr)
                    successor = successor->m_left;

                auto v = successor->m_value;
                auto k = successor->m_key;
                remove(successor->m_key);
                node->m_value = v;
                node->m_key = k;

                return;
            }
            else
            {
                auto child = (node->m_left != nullptr) ? node->m_left : node->m_right;

                if (node != m_root)
                {
                    if (node == node->m_parent->m_left)
                        node->m_parent->m_left = child;
                    else
                        node->m_parent->m_right = child;

                    child->m_parent = node->m_parent;
                }
                else
                    m_root = child;
            }

            std::free(node);
        }

    private:
        void helper_insert(node_t *&node, node_t *&parent, const K &key, const V &value)
        {
            if (node == nullptr)
            {
                node = new node_t(key, value, parent);
                return;
            }

            if (node->m_key == key)
            {
                node->m_value = value;
                return;
            }
            else if (node->m_key > key)
            {
                helper_insert(node->m_left, node, key, value);
            }
            else
            {
                helper_insert(node->m_right, node, key, value);
            }
        }

        node_t *helper_search(node_t *node, const K &key)
        {
            while (node != nullptr)
            {
                if (node->m_key == key)
                    return node;

                if (node->m_key > key)
                    node = node->m_left;
                else
                    node = node->m_right;
            }

            return nullptr;
        }

};
