all: test

test: test_main.o test_bst.o
	g++ -g -O0 -std=c++20 -o $@ $^

%.o: %.cpp
	g++ -g -Werror -O0 -std=c++20 -o $@ -c $<

test_bst.o: test_main.cpp bst_base.hpp util.hpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
